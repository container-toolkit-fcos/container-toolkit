# Copyright (c) 2019-2022, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include:
  - .common-ci.yml

variables:
  ACCESS_KEY: "" # Protected/masked variable.  Provides write ability to this repo.
  KEYCARD_PIN: "" # Protected/masked variable.  PIN to the gpg card in the usb slot of the runner.
  # Set these in your CICD variables if your gitlab-runner is behind a proxy.
  HTTP_PROXY: ""
  HTTPS_PROXY: ""
  NO_PROXY: "docker"
  # We set these from the capitalized versions to be sure to be sure...
  http_proxy: ${HTTP_PROXY}
  https_proxy: ${HTTPS_PROXY}
  no_proxy: ${NO_PROXY}

build-dev-image:
  stage: image
  script:
    - apk --no-cache add make bash
    - make .build-image
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - make .push-build-image

stages:
  - trigger
  - image
  - lint
  - go-checks
  - go-build
  # - unit-tests
  - package-build
  - package-sign-commit
  - image-build
  - test
  - scan
  - release

.requires-build-image:
  image: "${BUILDIMAGE}"

.go-check:
  extends:
    - .requires-build-image
  stage: go-checks

fmt:
  extends:
    - .go-check
  script:
    - make assert-fmt

vet:
  extends:
    - .go-check
  script:
    - make vet

lint:
  extends:
    - .go-check
  script:
    - make lint
  allow_failure: true

ineffassign:
  extends:
    - .go-check
  script:
    - make ineffassign
  allow_failure: true

misspell:
  extends:
    - .go-check
  script:
    - make misspell

go-build:
  extends:
    - .requires-build-image
  stage: go-build
  script:
    - make build

# unit-tests:
#   extends:
#     - .requires-build-image
#   stage: unit-tests
#   script:
#     - make coverage

# Define the package build helpers
.multi-arch-build:
  before_script:
    - apk add --no-cache coreutils build-base sed git bash make
    - '[[ -n "${SKIP_QEMU_SETUP}" ]] || docker run --rm --privileged multiarch/qemu-user-static --reset -p yes -c yes || true'

.package-artifacts:
  variables:
    ARTIFACTS_NAME: "toolkit-container-${CI_PIPELINE_ID}"
    ARTIFACTS_ROOT: "toolkit-container-${CI_PIPELINE_ID}"
    DIST_DIR: ${CI_PROJECT_DIR}/${ARTIFACTS_ROOT}

.package-build:
  extends:
    - .multi-arch-build
    - .package-artifacts
  stage: package-build
  timeout: 3h
  script:
    - ./scripts/build-packages.sh ${DIST}-${ARCH}

  artifacts:
    name: ${ARTIFACTS_NAME}
    paths:
      - ${ARTIFACTS_ROOT}

# Define the package build targets
# package-amazonlinux2-aarch64:
#   extends:
#     - .package-build
#     - .dist-amazonlinux2
#     - .arch-aarch64

# package-amazonlinux2-x86_64:
#   extends:
#     - .package-build
#     - .dist-amazonlinux2
#     - .arch-x86_64

# package-centos7-ppc64le:
#   extends:
#     - .package-build
#     - .dist-centos7
#     - .arch-ppc64le

package-centos7-x86_64:
  extends:
    - .package-build
    - .dist-centos7
    - .arch-x86_64

# package-centos8-aarch64:
#   extends:
#     - .package-build
#     - .dist-centos8
#     - .arch-aarch64

# package-centos8-ppc64le:
#   extends:
#     - .package-build
#     - .dist-centos8
#     - .arch-ppc64le

package-centos8-x86_64:
  extends:
    - .package-build
    - .dist-centos8
    - .arch-x86_64

package-fedora37-aarch64:
  extends:
    - .package-build
    - .dist-fedora37
    - .arch-aarch64

package-fedora38-aarch64:
  extends:
    - .package-build
    - .dist-fedora38
    - .arch-aarch64

package-fedora37-x86_64:
  extends:
    - .package-build
    - .dist-fedora37
    - .arch-x86_64

package-fedora38-x86_64:
  extends:
    - .package-build
    - .dist-fedora38
    - .arch-x86_64

# package-debian10-amd64:
#   extends:
#     - .package-build
#     - .dist-debian10
#     - .arch-amd64

# package-debian9-amd64:
#   extends:
#     - .package-build
#     - .dist-debian9
#     - .arch-amd64

# package-opensuse-leap15.1-x86_64:
#   extends:
#     - .package-build
#     - .dist-opensuse-leap15.1
#     - .arch-x86_64

# package-ubuntu16.04-amd64:
#   extends:
#     - .package-build
#     - .dist-ubuntu16.04
#     - .arch-amd64

# package-ubuntu16.04-ppc64le:
#   extends:
#     - .package-build
#     - .dist-ubuntu16.04
#     - .arch-ppc64le

# package-ubuntu18.04-amd64:
#   extends:
#     - .package-build
#     - .dist-ubuntu18.04
#     - .arch-amd64

# package-ubuntu18.04-arm64:
#   extends:
#     - .package-build
#     - .dist-ubuntu18.04
#     - .arch-arm64

# package-ubuntu18.04-ppc64le:
#   extends:
#     - .package-build
#     - .dist-ubuntu18.04
#     - .arch-ppc64le

.buildx-setup:
  before_script:
    -  export BUILDX_VERSION=v0.6.3
    -  apk add --no-cache curl
    -  mkdir -p ~/.docker/cli-plugins
    -  curl -sSLo ~/.docker/cli-plugins/docker-buildx "https://github.com/docker/buildx/releases/download/${BUILDX_VERSION}/buildx-${BUILDX_VERSION}.linux-amd64"
    -  chmod a+x ~/.docker/cli-plugins/docker-buildx
    -  docker context create tls-env
    # https://github.com/docker/buildx/issues/136#issuecomment-550205439
    -  docker buildx create --use tls-env --driver-opt env.http_proxy=$http_proxy --driver-opt env.https_proxy=$https_proxy --driver-opt env.no_proxy=$no_proxy --platform=linux/amd64,linux/arm64

    -  '[[ -n "${SKIP_QEMU_SETUP}" ]] || docker run --rm --privileged multiarch/qemu-user-static --reset -p yes || true'

# Define the image build targets
.image-build:
  stage: image-build
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/container-toolkit"
    VERSION: "${CI_COMMIT_SHORT_SHA}"
    PUSH_ON_BUILD: "true"
  before_script:
    - !reference [.buildx-setup, before_script]

    - apk add --no-cache bash make git
    - 'echo "Logging in to CI registry ${CI_REGISTRY}"'
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - make -f build/container/Makefile build-${DIST}

image-centos7:
  extends:
    - .image-build
    - .package-artifacts
    - .dist-centos7
  needs:
    # - package-centos7-aarch64
    - package-centos7-x86_64
    # - package-centos7-ppc64le

# image-centos8:
#   extends:
#     - .image-build
#     - .package-artifacts
#     - .dist-centos8
#   needs:
#     # - package-centos8-aarch64
#     - package-centos8-x86_64
#     # - package-centos8-ppc64le

# image-ubi8:
#   extends:
#     - .image-build
#     - .package-artifacts
#     - .dist-ubi8
#   needs:
#     # Note: The ubi8 image uses the centos8 packages
#     - package-centos8-aarch64
#     - package-centos8-x86_64
#     - package-centos8-ppc64le

# image-ubuntu18.04:
#   extends:
#     - .image-build
#     - .package-artifacts
#     - .dist-ubuntu18.04
#   needs:
#     - package-ubuntu18.04-amd64
#     - package-ubuntu18.04-arm64
#     - package-ubuntu18.04-ppc64le

# image-ubuntu20.04:
#   extends:
#     - .image-build
#     - .package-artifacts
#     - .dist-ubuntu20.04
#   needs:
#     - package-ubuntu18.04-amd64
#     - package-ubuntu18.04-arm64
#     - package-ubuntu18.04-ppc64le

# The DIST=packaging target creates an image containing all built packages
image-packaging:
  extends:
    - .image-build
    - .package-artifacts
    - .dist-packaging
  needs:
    # - job: package-centos8-aarch64
    - job: package-centos8-x86_64
    # - job: package-ubuntu18.04-amd64
    # - job: package-ubuntu18.04-arm64
    # - job: package-amazonlinux2-aarch64
    #   optional: true
    # - job: package-amazonlinux2-x86_64
    #   optional: true
    # - job: package-centos7-ppc64le
    #   optional: true
    # - job: package-centos7-x86_64
    #   optional: true
    # - job: package-centos8-ppc64le
    #   optional: true
    # - job: package-debian10-amd64
    #   optional: true
    # - job: package-debian9-amd64
    #   optional: true
    # - job: package-fedora35-aarch64
    #   optional: true
    # - job: package-fedora35-x86_64
    #   optional: true
    - job: package-fedora37-aarch64
      optional: true
    - job: package-fedora37-x86_64
      optional: true
    - job: package-fedora38-aarch64
      optional: true
    - job: package-fedora38-x86_64
      optional: true
    # - job: package-opensuse-leap15.1-x86_64
    #   optional: true
    # - job: package-ubuntu16.04-amd64
    #   optional: true
    # - job: package-ubuntu16.04-ppc64le
    #   optional: true
    # - job: package-ubuntu18.04-ppc64le
    #   optional: true

# Define publish test helpers
.test:toolkit:
  extends:
    - .integration
  variables:
    TEST_CASES: "toolkit"

.test:docker:
  extends:
    - .integration
  variables:
    TEST_CASES: "docker"

.test:containerd:
  # TODO: The containerd tests fail due to issues with SIGHUP.
  # Until this is resolved with retry up to twice and allow failure here.
  retry: 2
  allow_failure: true
  extends:
    - .integration
  variables:
    TEST_CASES: "containerd"

.test:crio:
  extends:
    - .integration
  variables:
    TEST_CASES: "crio"

# Define the test targets
# test-toolkit-ubuntu18.04:
#   extends:
#     - .test:toolkit
#     - .dist-ubuntu18.04
#   needs:
#     - image-ubuntu18.04

# test-containerd-ubuntu18.04:
#   extends:
#     - .test:containerd
#     - .dist-ubuntu18.04
#   needs:
#     - image-ubuntu18.04

# test-crio-ubuntu18.04:
#   extends:
#     - .test:crio
#     - .dist-ubuntu18.04
#   needs:
#     - image-ubuntu18.04

# test-docker-ubuntu18.04:
#   extends:
#     - .test:docker
#     - .dist-ubuntu18.04
#   needs:
#     - image-ubuntu18.04

# test-toolkit-ubuntu20.04:
#   extends:
#     - .test:toolkit
#     - .dist-ubuntu20.04
#   needs:
#     - image-ubuntu20.04

# test-containerd-ubuntu20.04:
#   extends:
#     - .test:containerd
#     - .dist-ubuntu20.04
#   needs:
#     - image-ubuntu20.04

# test-crio-ubuntu20.04:
#   extends:
#     - .test:crio
#     - .dist-ubuntu20.04
#   needs:
#     - image-ubuntu20.04

# test-docker-ubuntu20.04:
#   extends:
#     - .test:docker
#     - .dist-ubuntu20.04
#   needs:
#     - image-ubuntu20.04

.process-artifacts-setup: &process-artifacts-setup
  stage: package-sign-commit
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+.*fedora/'
      when: always
    - if: $CI_MERGE_REQUEST_ID
      when: manual
  variables:
    ARTIFACTS_DIR: "toolkit-container-${CI_PIPELINE_ID}"
  needs:
    - package-fedora37-x86_64
    - package-fedora37-aarch64
    - package-fedora38-x86_64
    - package-fedora38-aarch64
  tags:
    - robo-signer

.fedora-gpg-signing-setup: &fedora-gpg-rpm-signing-setup
  extends: .process-artifacts-setup
  # Latest stable version, ie. F34 at the time of writing but will move forward in time...
  image: fedora:34
  before_script:
    - dnf update -yq
    - dnf install -yq createrepo rpm-build rpm-sign yubikey-manager git findutils
    # https://ludovicrousseau.blogspot.com/2019/06/gnupg-and-pcsc-conflicts.html
    - mkdir -p ~/.gnupg; chmod go-rwx ~/.gnupg; echo "--disable-ccid" > ~/.gnupg/scdaemon.conf

.sign-artifacts-script: &sign-artifacts-script
  # GPG Init, ensuring a card is present and then importing corresponding public key.
  - while ! (gpg --card-status 2>&1 > /dev/null); do sleep 10; echo "Please insert keycard..."; done
  - public_key_url=$(gpg --card-status | grep 'URL of public key' | cut -d ':' -f2,3)
  - curl -s ${public_key_url} > /tmp/public_key.gpg
  - gpg --import /tmp/public_key.gpg
  - gpg --list-keys --fingerprint | grep pub -A 1 | egrep -Ev "pub|--" | tr -d ' ' | awk 'BEGIN { FS = "\n" } ; { print $1":6:" } ' | gpg --import-ownertrust
  - gpg --card-status
  # TODO - What happens if card has >1 keys present?
  - SIGNING_KEY=$(gpg --card-status | grep ssb\> | cut -d '/' -f2 | cut -d ' ' -f1)
  # Escaping % with %.
  - printf "%%_signature gpg\n%%_gpg_name ${SIGNING_KEY}" > /root/.rpmmacros
  # Git init with user name of person initiating pipeline...
  - git config --global user.name "${GITLAB_USER_NAME}"
  - git config --global user.email "${GITLAB_USER_EMAIL}"
  - git fetch
  - git checkout gl-pages
  - git pull
  # GPG Unlock card
  # TODO - Better handling of keycard PIN, currently a protected runner environment variable
  #        Maybe shard it and/or use Vault (ensuring VPN connectivity present)
  - touch /tmp/blah
  - echo "Please touch keycard..."
  - echo ${KEYCARD_PIN} | gpg --batch --pinentry-mode loopback --command-fd 0 --sign /tmp/blah
  # Create signed artifacts in /temp before moving to /stable.
  # When they are in /stable commit them to Gitlab causing another Pages pipeline to publish...
  - mkdir -p ./temp
  - cp -r ${ARTIFACTS_DIR}/fedora* ./temp
  # Prune excess files...non-rpms...
  - find ./temp -type d | grep lib | xargs -n1 rm -rf || true
  - find ./temp -name *.rpm | xargs rpm -addsign
  - |
    for repodir in $(find ./temp -type d -mindepth 2 -maxdepth 2)
    do
      pushd $repodir
      createrepo .
      rm -f ./repodata/repomd.xml.asc
      echo "" | gpg --batch --pinentry-mode loopback --command-fd 0 --detach-sign --armor ./repodata/repomd.xml
      popd
    done
  - cp -r ./temp/* ./stable
  - rm -rf ./temp

.commit-artifacts-script: &commit-artifacts-script
  # When artifacts are committed to the gl-pages branch it triggers a separate pipeline run...
  - git add ./stable
  - git status
  - git commit -S -m "Signed rpms generated by pipeline ${CI_PIPELINE_ID} triggered by tag ${CI_COMMIT_TAG}."
  - set -x
  - git push https://root:${ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git

sign-commit-artifacts:
  <<: *fedora-gpg-rpm-signing-setup
  script:
    - *sign-artifacts-script
    - *commit-artifacts-script
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+.*fedora/
      when: always

# Tags like 1.1.1-test allow a test build with rpm sign to be performed without
# committing the artifacts to the gl-pages branch.  Furthermore we sleep 900 in
# case we want to step into the build conntainer to diagnose environmental issues.
sign-artifacts:
  <<: *fedora-gpg-rpm-signing-setup
  script:
    - *sign-artifacts-script
    - sleep 900
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^v[0-9]+\.[0-9]+\.[0-9]+.*fedora/
      when: always
